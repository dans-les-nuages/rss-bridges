<?php

class RTEBridge extends BridgeAbstract
{
  const NAME = 'RTE emplois Bridge';
  const BASE_URI = 'https://www.rte-france.com';
  const URI = self::BASE_URI . '/carrieres/nos-offres';
  const DESCRIPTION = 'Bridge pour suivre les emplois chez RTE';
  const MAINTAINER = 'DevSecOpsTools';
  const CACHE_TIMEOUT = 25200; // 7h
  const ICON = 'https://assets.rte-france.com/prod/favicon_5.ico';

  public function getName() {
    return self::NAME;
  }
  public function getIcon() {
    return self::ICON;
  }

  public function collectData()
  {
    $dom = getSimpleHTMLDOM($this->getURI());
    foreach ($dom->find('a.box-read-more') as $job) {

      $content = "content";
      $title = $job->find('h3.read-more-title',0)->plaintext;
      $uri = static::BASE_URI . '/' . $job->href;
      $city = $job->find('div.read-more-localisation',0)->plaintext;

      $item = [];
      $item['content'] = $content;
      $item['title'] = $title;
      $item['uri'] = $uri;
      $item['city'] = $city;
      $this->items[] = $item;
    }
  }

}
